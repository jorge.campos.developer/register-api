package com.geradordedevs.registerapi.registerapi.facades;

import com.geradordedevs.registerapi.registerapi.dtos.requests.UserRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.requests.UserUpdateRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.responses.UserResponseDTO;

import java.util.List;

public interface UserFacade {

    UserResponseDTO save(UserRequestDTO userRequestDTO);
    UserResponseDTO findById(String id, String token);
    List<UserResponseDTO> findAll(String token);
    UserResponseDTO update(String id, UserUpdateRequestDTO userUpdateRequestDTO, String token);
    void deleteById(String id, String token);
}
