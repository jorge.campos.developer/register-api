package com.geradordedevs.registerapi.registerapi.facades.impl;

import com.geradordedevs.registerapi.registerapi.dtos.requests.UserRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.requests.UserUpdateRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.responses.UserResponseDTO;
import com.geradordedevs.registerapi.registerapi.entities.UserAddressEntity;
import com.geradordedevs.registerapi.registerapi.entities.UserEntity;
import com.geradordedevs.registerapi.registerapi.exceptions.UserException;
import com.geradordedevs.registerapi.registerapi.exceptions.enums.UserEnum;
import com.geradordedevs.registerapi.registerapi.facades.UserFacade;
import com.geradordedevs.registerapi.registerapi.services.UserService;
import com.geradordedevs.registerapi.registerapi.utils.JwtGenerator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserFacadeImpl implements UserFacade {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtGenerator jwtGenerator;

    @Override
    public UserResponseDTO save(UserRequestDTO userRequestDTO) {
        Optional<UserEntity> optional = userService.findByEmail(userRequestDTO.getEmail());

        if (optional.isPresent()) {
            throw new UserException(UserEnum.USER_ALREADY_EXIST);

        } else if (userRequestDTO.getPassword() == null || userRequestDTO.getPassword().isEmpty()){
            throw new UserException(UserEnum.PASSWORD_IS_REQUIRED);
        }

        userRequestDTO.setPassword(passwordEncoder.encode(userRequestDTO.getPassword()));
        return convertUserEntityToUserResponseDTO(userService.save(convertUserRequestDTOToUserEntity(userRequestDTO)));
    }


    @Override
    public UserResponseDTO findById(String id, String token) {
        jwtGenerator.validateJsonWebToken(token);
        return convertUserEntityToUserResponseDTO(userService.findById(id));
    }

    @Override
    public List<UserResponseDTO> findAll(String token) {
        jwtGenerator.validateJsonWebToken(token);
        List<UserResponseDTO> userResponseList = new ArrayList<>();
        userService.findAll().forEach(userEntity -> userResponseList.add(convertUserEntityToUserResponseDTO(userEntity)));
        return userResponseList;
    }

    @Override
    public UserResponseDTO update(String id, UserUpdateRequestDTO userUpdateRequestDTO, String token) {
        jwtGenerator.validateJsonWebToken(token);
        return convertUserEntityToUserResponseDTO(userService.update(id, convertUserUpdateRequestDTOToUserEntity(userUpdateRequestDTO)));
    }

    @Override
    public void deleteById(String id, String token) {
        jwtGenerator.validateJsonWebToken(token);
        userService.deleteById(id);
    }

    private UserResponseDTO convertUserEntityToUserResponseDTO(UserEntity userEntity) {
        return modelMapper.map(userEntity, UserResponseDTO.class);
    }

    private UserEntity convertUserRequestDTOToUserEntity(UserRequestDTO userRequestDTO) {
        return modelMapper.map(userRequestDTO, UserEntity.class);
    }

    private UserEntity convertUserUpdateRequestDTOToUserEntity(UserUpdateRequestDTO userUpdateRequestDTO) {
        return modelMapper.map(userUpdateRequestDTO , UserEntity.class);
    }
}
