package com.geradordedevs.registerapi.registerapi.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum TokenEnum {

    ERROR_TO_GENERATE_TOKEN("RGT_TKN_001", "failed trying to generate access token", 400),
    ACCESS_TOKEN_INVALID("RGT_TKN_002", "invalid token", 404);

    private String code;
    private String message;
    private Integer statusCode;
}
