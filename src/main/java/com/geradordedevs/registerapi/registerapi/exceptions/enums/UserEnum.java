package com.geradordedevs.registerapi.registerapi.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum UserEnum {

    USER_NOT_FOUND("USER_NOT_FOUND","User not found",404),
    USER_ALREADY_EXIST("USER_ALREADY_EXIST", "E-mail already registered", 400),
    PASSWORD_IS_REQUIRED("PASSWORD_IS_REQUIRED", "password is required",401);

    private String code;
    private String message;
    private Integer statusCode;
}
