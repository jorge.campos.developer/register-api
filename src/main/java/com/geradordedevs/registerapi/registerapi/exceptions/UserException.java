package com.geradordedevs.registerapi.registerapi.exceptions;

import com.geradordedevs.registerapi.registerapi.exceptions.enums.UserEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class UserException extends RegisterException {

    private static final long serialVersionUID = -4589179341768493322L;

    private UserEnum error;

    public UserException(UserEnum error) {
        super(error.getMessage());
        this.error = error;
    }
}
