package com.geradordedevs.registerapi.registerapi.exceptions;

import com.geradordedevs.registerapi.registerapi.exceptions.enums.TokenEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class TokenException extends RegisterException{

    private static final long serialVersionUID = -4589179341768493322L;

    private TokenEnum error;

    public TokenException(TokenEnum error) {
        super(error.getMessage());
        this.error = error;
    }
}
