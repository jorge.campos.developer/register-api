package com.geradordedevs.registerapi.registerapi.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailRequestDTO {

    private String from;
    private String to;
    private String subject;
    private String content;
}
