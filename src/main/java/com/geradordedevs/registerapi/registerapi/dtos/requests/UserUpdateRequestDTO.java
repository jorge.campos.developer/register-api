package com.geradordedevs.registerapi.registerapi.dtos.requests;

import com.geradordedevs.registerapi.registerapi.enums.UserEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateRequestDTO {

    private String firstName;
    private String lastName;
    private String email;
    private UserEnum type;

    private UserAddressRequestDTO address;
}
