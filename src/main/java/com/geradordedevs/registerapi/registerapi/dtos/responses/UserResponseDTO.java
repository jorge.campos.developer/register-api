package com.geradordedevs.registerapi.registerapi.dtos.responses;

import com.geradordedevs.registerapi.registerapi.entities.UserAddressEntity;
import com.geradordedevs.registerapi.registerapi.enums.UserEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDTO {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private UserEnum type;

    private UserAddressEntity address;
}
