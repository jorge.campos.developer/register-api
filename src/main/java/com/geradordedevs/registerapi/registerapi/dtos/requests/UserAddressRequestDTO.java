package com.geradordedevs.registerapi.registerapi.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAddressRequestDTO {

    private String street;
    private String number;
    private String neighborhood;
    private String complement;
    private String city;
    private String state;
    private String postalCode;
}
