package com.geradordedevs.registerapi.registerapi.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.geradordedevs.registerapi.registerapi.exceptions.TokenException;
import com.geradordedevs.registerapi.registerapi.exceptions.enums.TokenEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class JwtGenerator {

    @Value("${auth.util.jwt.secret}")
    private String SECRET;
    private static final String KEY_EMAIL = "EMAIL";
    private static final String USER_ID = "USER_ID";
    private final Date EXPIRES_AT = new Date(Instant.now().toEpochMilli() + TimeUnit.HOURS.toMillis(1)); // 1h

    public String createJsonWebToken(String email, String userId) {
        try {
            log.info("generating jwt token for user {}", email);

            return JWT.create()
                    .withIssuer("gerador-de-devs")
                    .withClaim(KEY_EMAIL, email)
                    .withClaim(USER_ID, userId)
                    .withExpiresAt(EXPIRES_AT)
                    .sign(Algorithm.HMAC256(SECRET));

        } catch (JWTCreationException exception){
            log.warn("error to generate  access token user {}", email);
            throw new TokenException(TokenEnum.ERROR_TO_GENERATE_TOKEN);
        }
    }

    public void validateJsonWebToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier jwtVerifier = JWT.require(algorithm).withIssuer("gerador-de-devs").build();
            jwtVerifier.verify(token);
        } catch (Exception e) {
            log.warn("invalid token {}", token);
            throw new TokenException(TokenEnum.ACCESS_TOKEN_INVALID);
        }
    }
}
