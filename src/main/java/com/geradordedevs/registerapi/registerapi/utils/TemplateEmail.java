package com.geradordedevs.registerapi.registerapi.utils;

import org.springframework.stereotype.Component;

@Component
public class TemplateEmail {

    public String sendContentSaveUser() {
        return new String("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Document</title>\n" +
                "    <style>\n" +
                "        @import url('https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap');\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body style=\"padding: 0; margin: 0;\">\n" +
                "    <table style=\"text-align: center ; background-color: #ECECEC; width: 55%; height: 587px;\">\n" +
                "        <tr>\n" +
                "            <td style=\"margin-top: 70px; display: inline-block; width: 600px; height: 341px; border-radius: 12px; background-color: #F5F5F5;\">\n" +
                "                <h2 style=\"margin-top: 58px; margin-left: 248px; width: 106px; height: 39px; font-size: 29px; font-family: 'Quicksand', sans-serif; font-weight: 600; line-height: 37px; color: #628D96; text-transform: capitalize;\">GD</h2>\n" +
                "                <h4 style=\"margin-top: 30px; margin-left: 200px; width: 200px; height: 22px; font-family: 'Quicksand', sans-serif; font-weight: 600; font-size: 17px; color: #334E68\">Cadastro concluído com sucesso. Parabéns!</h4>\n" +
                "                <div style=\"margin-top: 75px; margin-bottom: -40px; text-align: center; width: 600px; height: 24px; font-family: sans-serif; font-weight: 400, regular; font-size: 15px; color: #363636; cursor: pointer;\">\n" +
                "                    <a style=\"text-decoration: none; color: #363636;\" href=\"http://www.geradordedevs.com.br\">www.geradordedevs.com.br</a>\n" +
                "                    <div style=\"margin-top: 27px; width: 600px; height: 22px; font-family: Quicksand, sans-serif; font-weight: 400, regular; font-size: 14px; color: #A7A7A7\">Copyright © 2022 Website</div>\n" +
                "                </div>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "</body>\n" +
                "</html>");
    }

    public String sendContentUpdateUser() {
        return new String("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Document</title>\n" +
                "    <style>\n" +
                "        @import url('https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap');\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body style=\"padding: 0; margin: 0;\">\n" +
                "    <table style=\"text-align: center ; background-color: #ECECEC; width: 55%; height: 587px;\">\n" +
                "        <tr>\n" +
                "            <td style=\"margin-top: 70px; display: inline-block; width: 600px; height: 341px; border-radius: 12px; background-color: #F5F5F5;\">\n" +
                "                <h2 style=\"margin-top: 58px; margin-left: 248px; width: 106px; height: 39px; font-size: 29px; font-family: 'Quicksand', sans-serif; font-weight: 600; line-height: 37px; color: #628D96; text-transform: capitalize;\">GD</h2>\n" +
                "                <h4 style=\"margin-top: 30px; margin-left: 200px; width: 200px; height: 22px; font-family: 'Quicksand', sans-serif; font-weight: 600; font-size: 17px; color: #334E68\">Cadastro atualizado com sucesso. Parabéns!</h4>\n" +
                "                <div style=\"margin-top: 75px; margin-bottom: -40px; text-align: center; width: 600px; height: 24px; font-family: sans-serif; font-weight: 400, regular; font-size: 15px; color: #363636; cursor: pointer;\">\n" +
                "                    <a style=\"text-decoration: none; color: #363636;\" href=\"http://www.geradordedevs.com.br\">www.geradordedevs.com.br</a>\n" +
                "                    <div style=\"margin-top: 27px; width: 600px; height: 22px; font-family: Quicksand, sans-serif; font-weight: 400, regular; font-size: 14px; color: #A7A7A7\">Copyright © 2022 Website</div>\n" +
                "                </div>\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "</body>\n" +
                "</html>");
    }
}
