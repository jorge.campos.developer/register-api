package com.geradordedevs.registerapi.registerapi.controllers;

import com.geradordedevs.registerapi.registerapi.dtos.requests.UserRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.requests.UserUpdateRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.responses.UserResponseDTO;
import com.geradordedevs.registerapi.registerapi.facades.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/users")
public class UserController{

    @Autowired
    private UserFacade userFacade;

    @PostMapping
    public ResponseEntity<UserResponseDTO> save(@RequestBody UserRequestDTO userRequestDTO) {
        return new ResponseEntity<>(userFacade.save(userRequestDTO), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<UserResponseDTO>> findAll(@RequestHeader(required = false, value = "token") String token){
        return new ResponseEntity<>(userFacade.findAll(token), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDTO> findById(@PathVariable String id,
                                                    @RequestHeader(required = false, value = "token") String token ){
        return new ResponseEntity<>(userFacade.findById(id, token), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponseDTO> update(@PathVariable String id,
                                                  @RequestBody UserUpdateRequestDTO userUpdateRequestDTO,
                                                  @RequestHeader(required = false, value = "token") String token ){
        return new ResponseEntity<>(userFacade.update(id, userUpdateRequestDTO, token), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable String id,
                                           @RequestHeader(required = false, value = "token") String token )  {
        userFacade.deleteById(id, token);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
