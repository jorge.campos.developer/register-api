package com.geradordedevs.registerapi.registerapi.controllers;

import com.geradordedevs.registerapi.registerapi.dtos.requests.AuthenticationRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.responses.TokenResponseDTO;
import com.geradordedevs.registerapi.registerapi.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/authentication")
@CrossOrigin(origins = "*")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping
    public ResponseEntity<TokenResponseDTO> generateToken(@RequestBody AuthenticationRequestDTO authenticationRequestDTO) {
        return new ResponseEntity<>(authenticationService.authentication(authenticationRequestDTO), HttpStatus.CREATED);
    }
}
