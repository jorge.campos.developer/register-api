package com.geradordedevs.registerapi.registerapi.services.impl;

import com.geradordedevs.registerapi.registerapi.dtos.requests.AuthenticationRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.responses.TokenResponseDTO;
import com.geradordedevs.registerapi.registerapi.entities.UserEntity;
import com.geradordedevs.registerapi.registerapi.exceptions.TokenException;
import com.geradordedevs.registerapi.registerapi.exceptions.UserException;
import com.geradordedevs.registerapi.registerapi.exceptions.enums.TokenEnum;
import com.geradordedevs.registerapi.registerapi.exceptions.enums.UserEnum;
import com.geradordedevs.registerapi.registerapi.repositories.UserRepository;
import com.geradordedevs.registerapi.registerapi.services.AuthenticationService;
import com.geradordedevs.registerapi.registerapi.utils.JwtGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthenticationServiceImpl implements AuthenticationService{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtGenerator jwtGenerator;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public TokenResponseDTO authentication(AuthenticationRequestDTO authenticationRequestDTO) {
        UserEntity user = userRepository.findByEmail(authenticationRequestDTO.getEmail()).orElseThrow(() -> new UserException(UserEnum.USER_ALREADY_EXIST));

        if (encoder.matches(authenticationRequestDTO.getPassword(), user.getPassword())) {
            log.info("checking username and password of user {}", authenticationRequestDTO.getEmail());
            String token = jwtGenerator.createJsonWebToken(authenticationRequestDTO.getEmail(), authenticationRequestDTO.getPassword());
            jwtGenerator.validateJsonWebToken(token);
            return new TokenResponseDTO(token);
        } else {
            log.warn("{} username or password is invalid", authenticationRequestDTO.getEmail());
            throw new TokenException(TokenEnum.ERROR_TO_GENERATE_TOKEN);
        }
    }
}
