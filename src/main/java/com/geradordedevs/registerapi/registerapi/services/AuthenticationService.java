package com.geradordedevs.registerapi.registerapi.services;

import com.geradordedevs.registerapi.registerapi.dtos.requests.AuthenticationRequestDTO;
import com.geradordedevs.registerapi.registerapi.dtos.responses.TokenResponseDTO;

public interface AuthenticationService {

    TokenResponseDTO authentication(AuthenticationRequestDTO authenticationRequestDTO);
}
