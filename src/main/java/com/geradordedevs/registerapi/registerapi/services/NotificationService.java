package com.geradordedevs.registerapi.registerapi.services;

import com.geradordedevs.registerapi.registerapi.dtos.requests.EmailRequestDTO;

public interface NotificationService {

    void emailNotification(EmailRequestDTO emailRequestDTO);
}
