package com.geradordedevs.registerapi.registerapi.services;

import com.geradordedevs.registerapi.registerapi.entities.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserService {

    UserEntity save(UserEntity userEntity);
    UserEntity findById(String id);
    List<UserEntity> findAll();
    UserEntity update(String id, UserEntity userEntity);
    void deleteById(String id);
    Optional<UserEntity> findByEmail(String email);
}
