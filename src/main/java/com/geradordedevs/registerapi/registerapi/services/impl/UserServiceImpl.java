package com.geradordedevs.registerapi.registerapi.services.impl;

import com.geradordedevs.registerapi.registerapi.dtos.requests.EmailRequestDTO;
import com.geradordedevs.registerapi.registerapi.entities.UserEntity;
import com.geradordedevs.registerapi.registerapi.exceptions.UserException;
import com.geradordedevs.registerapi.registerapi.exceptions.enums.UserEnum;
import com.geradordedevs.registerapi.registerapi.repositories.UserRepository;
import com.geradordedevs.registerapi.registerapi.services.NotificationService;
import com.geradordedevs.registerapi.registerapi.services.UserService;
import com.geradordedevs.registerapi.registerapi.utils.TemplateEmail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TemplateEmail templateEmail;

    @Value("${email_from}")
    private String emailFrom;

    @Value("${email_subject}")
    private String emailSubject;

    @Override
    public UserEntity save(UserEntity userEntity) {
        log.info("save user in database");
        UserEntity user = userRepository.save(userEntity);
        user.setPassword("******");
        sendNotificationByEmail(userEntity);
        return user;
    }

    @Override
    public UserEntity findById(String id) {
        log.info("find user by id in database {}", id);
        return userRepository.findById(id).orElseThrow(() -> new UserException(UserEnum.USER_NOT_FOUND));
    }

    @Override
    public List<UserEntity> findAll() {
        log.info("find all users in the database");
        return userRepository.findAll();
    }

    @Override
    public UserEntity update(String id, UserEntity userEntity) {
        log.info("update user {}", id );
        userEntity.setId(id);
        sendNotificationUpdateByEmail(userEntity);
        return userRepository.save(userEntity);
    }

    @Override
    public void deleteById(String id) {
        log.info("delete user {}", id);
        this.userRepository.deleteById(id);
    }

    @Override
    public Optional<UserEntity> findByEmail(String email) {
        log.info("find user by email {}", email);
        return userRepository.findByEmail(email);
    }

    private void sendNotificationByEmail(UserEntity userEntity) {
        EmailRequestDTO emailRequestDTO = new EmailRequestDTO();
        emailRequestDTO.setTo(userEntity.getEmail());
        emailRequestDTO.setFrom(emailFrom);
        emailRequestDTO.setSubject(emailSubject);
        emailRequestDTO.setContent(templateEmail.sendContentSaveUser());
        notificationService.emailNotification(emailRequestDTO);
    }

    private void sendNotificationUpdateByEmail(UserEntity userEntity) {
        EmailRequestDTO emailRequestDTO = new EmailRequestDTO();
        emailRequestDTO.setTo(userEntity.getEmail());
        emailRequestDTO.setFrom(emailFrom);
        emailRequestDTO.setSubject(emailSubject);
        emailRequestDTO.setContent(templateEmail.sendContentUpdateUser());
        notificationService.emailNotification(emailRequestDTO);
    }

    private void verifyIfUserAlreadyExistInDataBase(String email) {
        if (userRepository.findByEmail(email).isPresent()) {
            throw new UserException(UserEnum.USER_ALREADY_EXIST);
        }
    }
}
