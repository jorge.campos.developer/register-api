package com.geradordedevs.registerapi.registerapi.services.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import com.geradordedevs.registerapi.registerapi.dtos.requests.EmailRequestDTO;
import com.geradordedevs.registerapi.registerapi.services.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class NotificationServiceImpl implements NotificationService {

    @Value("${CLOUD_AWS_ACCESS_KEY}")
    private String access_key;

    @Value("${CLOUD_AWS_SECRET_KEY}")
    private String secret_key;

    @Override
    public void emailNotification(EmailRequestDTO emailRequestDTO) {
        log.info("notification by email: {}", emailRequestDTO.getTo());

        AWSCredentials awsCredentials = new BasicAWSCredentials(access_key, secret_key);
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

        AmazonSimpleEmailService amazonSimpleEmailService = AmazonSimpleEmailServiceClientBuilder.standard().withCredentials(awsCredentialsProvider).withRegion(Regions.US_EAST_1).build();

        SendEmailRequest sendEmailRequest = new SendEmailRequest().withDestination(new Destination().withToAddresses(emailRequestDTO.getTo()))
                .withMessage(new Message().withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(emailRequestDTO.getContent())))
                        .withSubject(new Content().withCharset("UTF-8").withData(emailRequestDTO.getSubject()))).withSource(emailRequestDTO.getFrom());

        amazonSimpleEmailService.sendEmail(sendEmailRequest);
    }
}
