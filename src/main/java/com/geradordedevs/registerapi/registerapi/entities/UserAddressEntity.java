package com.geradordedevs.registerapi.registerapi.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAddressEntity {

    private String street;
    private String number;
    private String neighborhood;
    private String complement;
    private String city;
    private String state;
    private String postalCode;
}
