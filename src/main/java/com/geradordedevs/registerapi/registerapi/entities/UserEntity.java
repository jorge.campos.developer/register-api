package com.geradordedevs.registerapi.registerapi.entities;

import com.geradordedevs.registerapi.registerapi.enums.UserEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document("users")
public class UserEntity{

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private UserEnum type;

    private UserAddressEntity address;
}
